﻿using System.Collections.Generic;
using System.Linq;

namespace CalendarCupGenerator
{
    public static class CombinationUtils
    {
        public static void GenerateAllCombinations<T>(IReadOnlyList<T> set, int combinationSize, IList<T[]> allCombinations)
        {
            GenerateAllCombinations(set, set.Count, combinationSize, 0, new T[combinationSize], 0, allCombinations);
        }

        public static List<T[]> GenerateAllCombinations<T>(IReadOnlyList<T> set, int combinationSize)
        {
            var allCombinations = new List<T[]>();

            GenerateAllCombinations(set, set.Count, combinationSize, 0, new T[combinationSize], 0, allCombinations);

            return allCombinations;
        }

        private static void GenerateAllCombinations<T>(IReadOnlyList<T> arr,
              int setSize, int combinationSize, int index,
                      IList<T> data, int i, ICollection<T[]> allCombinations)
        {
            if (index == combinationSize)
            {
                allCombinations.Add(data.ToArray());
                return;
            }

            // When no more elements are there
            // to put in data[]
            if (i >= setSize)
                return;

            // current is included, put next
            // at next location
            data[index] = arr[i];
            GenerateAllCombinations(arr, setSize, combinationSize, index + 1,
                                    data, i + 1, allCombinations);

            // current is excluded, replace
            // it with next (Note that i+1 
            // is passed, but index is not
            // changed)
            GenerateAllCombinations(arr, setSize, combinationSize, index,
                                    data, i + 1, allCombinations);
        }
    }
}