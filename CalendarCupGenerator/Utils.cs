﻿namespace CalendarCupGenerator
{
    public static class Utils
    {
        public static bool IsValidMatchWeeksCombination(MatchWeek[] matchWeeksCombination)
        {
            for (int i = 0; i < matchWeeksCombination.Length; ++i)
            {
                var matchWeek = matchWeeksCombination[i];
                for (int j = i + 1; j < matchWeeksCombination.Length; ++j)
                {
                    var otherMatchWeek = matchWeeksCombination[j];
                    if (matchWeek.IsInConflictWith(otherMatchWeek))
                        return false;
                }
            }
            return true;
        }
    }
}