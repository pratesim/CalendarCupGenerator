﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CalendarCupGenerator
{
	class Program
	{
		private static Dictionary<int, Match[]> _constraints;
		private static IEnumerable<string> _clubs;
	    private static SheetReader _sheetReader;
        private const int RoundsNumber = 2;

        static void Main(string[] args)
		{
            var startTime = DateTime.Now;
            _sheetReader = new SheetReader();
            _sheetReader.SetLeagueCalendarFilePath("F:\\Dev\\Programs\\CalendarCupGenerator\\Calendario_Campionato.xlsx");

            SetupClubs();
			SetupConstraints();

            var allPossibleRoundsAB = GenerateAllPossibleRounds();

            IEnumerable<MatchWeek> cupRoundA = null;
            IEnumerable<MatchWeek> cupRoundB = null;

            foreach (var roundAB in allPossibleRoundsAB)
            {
                var clubsInRoundA = roundAB[0];
                var clubsInRoundB = roundAB[1];

                cupRoundA = GenerateCalendarsNotInConflictWithChampionship(clubsInRoundA).FirstOrDefault();
                cupRoundB = GenerateCalendarsNotInConflictWithChampionship(clubsInRoundB).FirstOrDefault();

                if (cupRoundA != null && cupRoundB != null)
                    break;
            }

            if (cupRoundA != null && cupRoundB != null)
            {
                Console.WriteLine("RoundA: " + string.Join(";", cupRoundA));
                Console.WriteLine("RoundB: " + string.Join(";", cupRoundB));
            }

            var endTime = DateTime.Now;
            var time = endTime - startTime;
            Console.WriteLine("Result Generated in: " + time);


            Console.WriteLine("Press enter to close...");
            Console.ReadLine();
        }

        private static void SetupClubs()
        {
            _clubs = _sheetReader.GetClubs();
        }

        private static void SetupConstraints()
        {
            _constraints = _sheetReader.GetLeagueMatches();
        }

        private static IEnumerable<string[][]> GenerateAllPossibleRounds()
        {
            var combinationsSize = _clubs.Count() / RoundsNumber;

            var allPossibleRounds = CombinationUtils.GenerateAllCombinations(_clubs.ToList(), combinationsSize);
            var allPossiblesRoundsAB = CombinationUtils.GenerateAllCombinations(allPossibleRounds, RoundsNumber);
            RemoveInvalidsRoundsAB(allPossiblesRoundsAB);

            return allPossiblesRoundsAB;
        }

        private static void RemoveInvalidsRoundsAB(IList<string[][]> roundsAB)
        {
            var tmpAllPossibleRoundsAB = new List<string[][]>();
            tmpAllPossibleRoundsAB = roundsAB.ToList();

            foreach (var possibleValidRoundAB in tmpAllPossibleRoundsAB)
            {
                var possibleRoundA = possibleValidRoundAB[0];
                var possibleRoundB = possibleValidRoundAB[1];

                foreach (var clubA in possibleRoundA)
                {
                    foreach (var clubB in possibleRoundB)
                    {
                        if (clubA == clubB)
                            roundsAB.Remove(possibleValidRoundAB);
                    }
                }
            }
        }

        private static IEnumerable<MatchWeek[]> GenerateCalendarsNotInConflictWithChampionship(IList<string> clubsInRound)
	    {
            var possibleMatches = GetAllPossibleMatches(clubsInRound);
	        var possibleMatchWeeks = GetAllPossibleMatchWeeks(possibleMatches, clubsInRound);            

            var possibleMatchWeeksCombinations = GenerateAllPossibleMatchWeeksCombinations(possibleMatchWeeks.ToArray(), clubsInRound.Count);
	        RemoveInvalidMatchWeeksCombinations(possibleMatchWeeksCombinations);
	        RemoveCalendarsConflictingWithChampionship(possibleMatchWeeksCombinations);

	        return possibleMatchWeeksCombinations;
	    }

        private static HashSet<Match> GetAllPossibleMatches(IList<string> clubsInRound)
        {
            var possibleMatches = new HashSet<Match>();

            for (var i = 0; i < clubsInRound.Count; i++)
            {
                var firstTeam = clubsInRound[i];
                for (var j = i + 1; j < clubsInRound.Count; j++)
                {
                    var secondTeam = clubsInRound[j];
                    possibleMatches.Add(new Match(firstTeam, secondTeam));
                }
            }
            return possibleMatches;
        }

        private static IEnumerable<MatchWeek> GetAllPossibleMatchWeeks(IEnumerable<Match> possibleMatches, IList<string> clubsInRound)
        {
            var possibleMatchesAsArray = possibleMatches.ToArray();
            var allPossibleMatchWeeks = new List<MatchWeek>();

            for (var i = 0; i < possibleMatchesAsArray.Length; ++i)
            {
                var firstMatch = possibleMatchesAsArray[i];
                for (var j = i + 1; j < possibleMatchesAsArray.Length; ++j)
                {
                    var secondMatch = possibleMatchesAsArray[j];
                    if (firstMatch.CanBePlayedInTheSameWeekOf(secondMatch))
                    {
                        var matchWeek = new MatchWeek { FirstMatch = firstMatch, SecondMatch = secondMatch };
                        matchWeek.RestingTeam = GetRestingTeam(matchWeek, clubsInRound);
                        allPossibleMatchWeeks.Add(matchWeek);
                    }
                }
            }
            return allPossibleMatchWeeks;
        }

        private static string GetRestingTeam(MatchWeek matchWeek, ICollection<string> clubRound)
        {
            string[] teamNotResting = { matchWeek.FirstMatch.FirstTeam, matchWeek.FirstMatch.SecondTeam, matchWeek.SecondMatch.FirstTeam, matchWeek.SecondMatch.SecondTeam };
            if (clubRound.Count % 2 == 0)
                return null;

            var restingTeam = clubRound.Except(teamNotResting).First();
            return restingTeam;
        }

        private static List<MatchWeek[]> GenerateAllPossibleMatchWeeksCombinations(IReadOnlyList<MatchWeek> allPossibleMatchWeeks, int numberOfMatchWeekInASeason)
        {
            return CombinationUtils.GenerateAllCombinations(allPossibleMatchWeeks, numberOfMatchWeekInASeason);
        }

        private static void RemoveInvalidMatchWeeksCombinations(List<MatchWeek[]> matchWeeksCombinations)
        {
            matchWeeksCombinations.RemoveAll(matchWeeksCombination => !Utils.IsValidMatchWeeksCombination(matchWeeksCombination));
        }

	    private static void RemoveCalendarsConflictingWithChampionship(List<MatchWeek[]> matchWeeksCombinations)
        {
            matchWeeksCombinations.RemoveAll(calendar => (MatchWeeksAreInConflictWithChampionship(calendar)));
        }

        private static bool MatchWeeksAreInConflictWithChampionship(IEnumerable<MatchWeek> matchWeeks)
	    {
	        var matchWeeksAsArray = matchWeeks.ToArray();
            if (matchWeeksAsArray.Length != _constraints.Count / 2)
                throw new Exception("Constraint and matchWeeks cannot have different size");

            var backAndForthCupRound = new MatchWeek[matchWeeksAsArray.Length * 2];
            matchWeeksAsArray.CopyTo(backAndForthCupRound, 0);

            var reversedCupRound = matchWeeksAsArray.Reverse();
            reversedCupRound.ToArray().CopyTo(backAndForthCupRound, matchWeeksAsArray.Length);

	        return backAndForthCupRound.Where((t, i) => _constraints[i].Any(item => t.FirstMatch == item || t.SecondMatch == item)).Any();
	    }
	}
}
