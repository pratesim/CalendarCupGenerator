﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CalendarCupGenerator
{
    public class Match: IEnumerable<string>, IEquatable<Match>
    {
        private readonly IList<string> _matches = new List<string>();
        private readonly string _firstTeam;
        private readonly string _secondTeam;

        public string FirstTeam {
            get { return _firstTeam; }
        }

        public string SecondTeam
        {
            get { return _secondTeam;}
        }

        public bool IsPartOfGame;

        public Match(string firstTeam, string secondTeam)
        {
            _firstTeam = firstTeam;
            _secondTeam = secondTeam;

            _matches.Add(_firstTeam);
            _matches.Add(_secondTeam);

            IsPartOfGame = false;
        }

        public override string ToString()
        {
            return FirstTeam + " - " + SecondTeam;
        }        

        public IEnumerator<string> GetEnumerator()
        {
            return _matches.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _matches.GetEnumerator();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Match) obj);
        }

        public bool Equals(Match otherMatch)
        {
            if (ReferenceEquals(null, otherMatch)) return false;
            if (ReferenceEquals(this, otherMatch)) return true;

            return FirstTeam == otherMatch.FirstTeam && SecondTeam == otherMatch.SecondTeam || FirstTeam == otherMatch.SecondTeam && SecondTeam == otherMatch.FirstTeam;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return 0;
            }
        }

        public static bool operator ==(Match left, Match right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Match left, Match right)
        {
            return !Equals(left, right);
        }

        public bool CanBePlayedInTheSameWeekOf(Match otherMatch)
        {
            foreach (var team in this)
            {
                foreach (var otherTeam in otherMatch)
                {
                    if (team == otherTeam)
                        return false;
                }
            }
            return true;
        }
    }
}