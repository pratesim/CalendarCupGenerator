﻿using System;

namespace CalendarCupGenerator
{
	public class MatchWeek
	{
		private Match _match1;
		private Match _match2;
		private string _restingTeam;

		public Match FirstMatch
		{
			get { return _match1; }
			set { _match1 = value; }
		}

		public Match SecondMatch
		{
			get { return _match2; }
			set { _match2 = value; }
		}

		public string RestingTeam
		{
			get { return _restingTeam; }
			set { _restingTeam = value; }
		}

	    public override string ToString()
	    {
	        return "{ " + FirstMatch + " ; " + SecondMatch + " }";
	    }

	    public bool IsInConflictWith(MatchWeek matchWeek)
	    {
	        if (matchWeek == null)
	            return false;

	        if (FirstMatch == matchWeek.FirstMatch ||
	            FirstMatch == matchWeek.SecondMatch ||
	            SecondMatch == matchWeek.FirstMatch ||
	            SecondMatch == matchWeek.SecondMatch)
	        {
	            return true;
	        }

	        return false;
	    }
	}
}