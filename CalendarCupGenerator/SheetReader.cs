﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToExcel;


namespace CalendarCupGenerator
{
    public class SheetReader
    {
        private ExcelQueryFactory _championshipCalendar;

        public SheetReader()
        {
            /*look at the API usage to query excel sheet https://github.com/paulyoder/LinqToExcel */                                 
            _championshipCalendar = new ExcelQueryFactory(@"..\..\resources\Calendario_Campionato.xlsx");
        }

        public void SetLeagueCalendarFilePath(string path)
        {
            _championshipCalendar = new ExcelQueryFactory(path);
        }

        public IEnumerable<string> GetClubs()
        {
            var clubsFromSheet = from c in _championshipCalendar.WorksheetRangeNoHeader("A5", "D9", "Calendario")
                                   select c;

            var clubs = new List<string>();
            foreach (var club in clubsFromSheet)
            {
                clubs.Add(club[0]);
                clubs.Add(club[3]);
            }

            return clubs;
        }

        public Dictionary<int, Match[]> GetLeagueMatches()
        {
            var matchesFromChampionship = from c in _championshipCalendar.WorksheetRangeNoHeader("G17", "J75", "Calendario")
                                          where c[1] != null
                                          select c;

            var matchesFromChampionshipAsArray = matchesFromChampionship.ToArray();

            var matches = new Dictionary<int, Match[]>();

            int j = 0;
            for (int i = 0; i < matchesFromChampionshipAsArray.Length; )
            {
                matches[j] = new Match[5];
                for (int k = 0; k < 5; k++)
                {
                    var firstTeam = (matchesFromChampionshipAsArray[i])[0];
                    var secondTeam = (matchesFromChampionshipAsArray[i])[3];
                    (matches[j])[k] = new Match(firstTeam, secondTeam);
                    ++i;
                }
                j++;
            }
            
            return matches;
        }

    }
}