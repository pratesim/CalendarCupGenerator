﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalendarCupGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarCupGenerator.Tests
{
    [TestClass()]
    public class MatchTests
    {
        [TestMethod()]
        public void EqualsTest1()
        {
            var firstMatch = new Match("A", "B");
            var secondMatch = new Match("A", "B");

            Assert.IsTrue(firstMatch.Equals(secondMatch));
        }
        [TestMethod()]
        public void EqualsTest2()
        {
            var firstMatch = new Match("A", "B");
            var secondMatch = new Match("B", "A");

            Assert.IsTrue(firstMatch.Equals(secondMatch));
        }
        [TestMethod()]
        public void EqualsTest3()
        {
            var firstMatch = new Match("A", "B");
            var secondMatch = new Match("B", "C");

            Assert.IsFalse(firstMatch.Equals(secondMatch));
        }
        [TestMethod()]
        public void EqualsTest4()
        {
            var firstMatch = new Match("A", "B");
            var secondMatch = new Match("B", "C");

            Assert.IsFalse(firstMatch.Equals(secondMatch));
        }

        [TestMethod()]
        public void EqualsTest5()
        {
            var firstMatch = new Match("A", "B");
            var secondMatch = new Match("C", "D");

            Assert.IsFalse(firstMatch.Equals(secondMatch));
        }
    }
}