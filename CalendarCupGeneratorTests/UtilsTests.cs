﻿using CalendarCupGenerator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalendarCupGeneratorTests
{
    [TestClass()]
    public class UtilsTests
    {
        [TestMethod()]
        public void IsValidMatchWeeksCombinationTest()
        {
            var matchWeek1 = new MatchWeek
            {
                FirstMatch = new Match("A", "B"),
                SecondMatch = new Match("C", "D"),
                RestingTeam = "E"
            };

            var matchWeek2 = new MatchWeek
            {
                FirstMatch = new Match("A", "B"),
                SecondMatch = new Match("D", "E"),
                RestingTeam = "C"
            };

            var marchWeeks = new[] {matchWeek1, matchWeek2};
            
            Assert.IsFalse(Utils.IsValidMatchWeeksCombination(marchWeeks));
        }

        [TestMethod()]
        public void IsValidMatchWeeksCombinationTest2()
        {
            var matchWeek1 = new MatchWeek
            {
                FirstMatch = new Match("A", "B"),
                SecondMatch = new Match("C", "D"),
                RestingTeam = "E"
            };

            var matchWeek2 = new MatchWeek
            {
                FirstMatch = new Match("A", "D"),
                SecondMatch = new Match("C", "E"),
                RestingTeam = "B"
            };

            var marchWeeks = new[] { matchWeek1, matchWeek2 };

            Assert.IsTrue(Utils.IsValidMatchWeeksCombination(marchWeeks));
        }

        [TestMethod()]
        public void IsValidMatchWeeksCombinationTest3()
        {           
            var matchWeek1 = new MatchWeek
            {
                FirstMatch = new Match("Top", "Raj"),
                SecondMatch = new Match("Blac", "Sal"),
                RestingTeam = "Spo"
            };

            var matchWeek2 = new MatchWeek
            {
                FirstMatch = new Match("Top", "Blac"),
                SecondMatch = new Match("Raj", "Sal"),
                RestingTeam = "Spo"
            };
            var matchWeek3 = new MatchWeek
            {
                FirstMatch = new Match("Top", "Blac"),
                SecondMatch = new Match("Raj", "Spo"),
                RestingTeam = "Sal"
            };

            var matchWeek4 = new MatchWeek
            {
                FirstMatch = new Match("Top", "Blac"),
                SecondMatch = new Match("Sal", "Spo"),
                RestingTeam = "Raj"
            };
            var matchWeek5 = new MatchWeek
            {
                FirstMatch = new Match("Top", "Sal"),
                SecondMatch = new Match("Raj", "Blac"),
                RestingTeam = "Spo"
            };

            var marchWeeks = new[] { matchWeek1, matchWeek2, matchWeek3, matchWeek4, matchWeek5 };

            Assert.IsFalse(Utils.IsValidMatchWeeksCombination(marchWeeks));
        }
    }
}